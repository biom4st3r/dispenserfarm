package com.biom4st3r.DispenserFarm;

import net.fabricmc.api.ModInitializer;
import net.minecraft.block.DispenserBlock;
import net.minecraft.item.Item;
import net.minecraft.item.Items;



public class DispenserFarmMod implements ModInitializer {
	public static final String MODID = "biom4st3rDispenserFarm";

	@Override
	public void onInitialize() 
	{
        
        Item[] cropItems = new Item[]{Items.CARROT,Items.WHEAT_SEEDS,Items.BEETROOT_SEEDS,Items.MELON_SEEDS,Items.PUMPKIN_SEEDS,Items.POTATO};
        //DispenserBlock.registerBehavior(Items.CARROT,new PlantSeedBehavior());//itemProvider_1, dispenserBehavior_1);
        for (Item i : cropItems) {
            DispenserBlock.registerBehavior(i, new PlantSeedBehavior());
        }
	}
}
