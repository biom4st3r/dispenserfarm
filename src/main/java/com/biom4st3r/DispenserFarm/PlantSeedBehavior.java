package com.biom4st3r.DispenserFarm;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.CarrotsBlock;
import net.minecraft.block.DispenserBlock;
import net.minecraft.block.EnchantingTableBlock;
import net.minecraft.block.FarmlandBlock;
import net.minecraft.block.dispenser.DispenserBehavior;
import net.minecraft.block.dispenser.ItemDispenserBehavior;
import net.minecraft.entity.ItemEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.BlockItem;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.nbt.ListTag;
import net.minecraft.nbt.StringTag;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.util.AbsoluteHand;
import net.minecraft.util.Hand;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.math.BlockPointer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.world.World;

public class PlantSeedBehavior extends  ItemDispenserBehavior {

    @Override
    public ItemStack dispenseStack(BlockPointer bp, ItemStack iS) 
    {

        World w = bp.getWorld();
        Direction direction_1 = (Direction)bp.getBlockState().get(DispenserBlock.FACING);

        BlockPos soilPos = direction_1 == Direction.DOWN ? bp.getBlockPos().offset(direction_1).offset(direction_1) : bp.getBlockPos().offset(direction_1);
        BlockState soilBlock = w.getBlockState(soilPos);
        BlockState cropBlock = w.getBlockState(soilPos.up());

        boolean unoccupied = cropBlock.getBlock() == Blocks.AIR;
        if(soilBlock.getBlock() == Blocks.FARMLAND && unoccupied)
        {
            BlockItem cropItem =  (BlockItem) iS.getItem();
            bp.getWorld().setBlockState(soilPos.up(), cropItem.getBlock().getDefaultState());
            iS.subtractAmount(1);
            return iS;
        }
        else
        {
            /*
            ItemStack book = new ItemStack(Items.WRITABLE_BOOK);
            ListTag lt = new ListTag();
            String s = "unoccupied is " + unoccupied + "\nBlockAbove is " + blockAbove.getBlock().toString() + "\nSoil is " + soil.toString();

            lt.add(new StringTag(s));
            book.setChildTag("pages", lt);
            bp.getWorld().spawnEntity(new ItemEntity(bp.getWorld(), bp.getX(), bp.getY(), bp.getZ(), book));
            */
            return new ItemDispenserBehavior().dispense(bp, iS);
        }
    }

    protected void playSound(BlockPointer blockPointer_1) 
    {
        blockPointer_1.getWorld().playLevelEvent(1000, blockPointer_1.getBlockPos(), 0);
    }

    protected void spawnParticles(BlockPointer blockPointer_1, Direction direction_1) 
    {
        blockPointer_1.getWorld().playLevelEvent(2000, blockPointer_1.getBlockPos(), direction_1.getId());
    }



}